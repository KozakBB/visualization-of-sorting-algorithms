const insertionSort = (array) => {
    const animations = [];

    for (let i = 1; i < array.length; i++) {
        // Choosing the first element in our unsorted subarray
        let current = array[i];
        // The last element of our sorted subarray
        let j = i-1; 

        //We push our selected values to animations array to execute step-by-step visualization in InsetionSortAnimation.js
        while ((j >= 0) && (current < array[j])) {
            array[j+1] = array[j];
            animations.push([(j+1), j]);
            j--;
        }
        array[j+1] = current;
    }
    return animations;
}

export default insertionSort;